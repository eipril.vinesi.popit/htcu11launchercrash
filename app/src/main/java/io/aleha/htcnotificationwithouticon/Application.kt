package io.aleha.htcnotificationwithouticon

import android.app.Application

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        NotificationHelper.createNotificationChannel(this)
    }

}