package io.aleha.htcnotificationwithouticon

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnWithIcon.setOnClickListener {
            ForegroundService.start(this, withIcon = true)
        }

        btnWithoutIcon.setOnClickListener {
            ForegroundService.start(this, withIcon = false)
        }
    }

}
