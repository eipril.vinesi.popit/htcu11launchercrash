package io.aleha.htcnotificationwithouticon

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import androidx.core.content.ContextCompat

class ForegroundService : Service() {

    override fun onBind(intent: Intent?): IBinder? = null

    private val handler = Handler()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            WITH_ICON -> showNotification(showIcon = true)
            WITHOUT_ICON -> showNotification(showIcon = false)
            else -> Unit
        }
        return START_NOT_STICKY
    }

    private fun showNotification(showIcon: Boolean) {
        val notification =
            NotificationHelper.createNotificationBuilder(this)
                .apply {
                    if (showIcon) {
                        setSmallIcon(R.drawable.sberbank_white)
                    }
                }
                .build()

        startForeground(13, notification)

        handler.postDelayed({
            stop()
        }, 3_000)
    }

    private fun stop() {
        stopForeground(true)
        stopSelf()
    }

    companion object {

        private const val WITH_ICON = "WITH_ICON"
        private const val WITHOUT_ICON = "WITHOUT_ICON"

        fun start(context: Context, withIcon: Boolean) {
            val intent = Intent(context, ForegroundService::class.java)
            intent.action = if (withIcon) WITH_ICON else WITHOUT_ICON
            ContextCompat.startForegroundService(context, intent)
        }
    }
}